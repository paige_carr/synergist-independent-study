package hello;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;


@Configuration
@EnableAutoConfiguration
public class Application {

	static String mailboxDestination = "mailbox-destination";

    @Bean
    Receiver receiver() {
        return new Receiver();
    }

	@Bean
	MessageListenerAdapter adapter(Receiver receiver) {
		return new MessageListenerAdapter(receiver) {
			{
				setDefaultListenerMethod("receiveMessage");
			}
		};
	}

	@Bean
	SimpleMessageListenerContainer container(final MessageListenerAdapter messageListener,
			final ConnectionFactory connectionFactory) {
		return new SimpleMessageListenerContainer() {
			{
				setMessageListener(messageListener);
				setConnectionFactory(connectionFactory);
				setDestinationName(mailboxDestination);
                setPubSubDomain(true);
			}
		};
	}

    public static void main(String[] args) {
        // Launch the application
        final ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        // Send a message
        MessageCreator message1 = new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage("Paige, Text Message Successfully Sent!");
            }
        };
    
        MessageCreator message2 = new MessageCreator() {        	
            //the message that will be sent
            public Message createMessage(Session session) throws JMSException {
                return session.createObjectMessage("This is an object message!!");
            }
        };
        MessageCreator message3 = new MessageCreator() {        	
            //the message that will be sent
        	
            public Message createMessage(Session session) throws JMSException {
            	String data="This is a byte message";
				BytesMessage message=session.createBytesMessage();
				message.writeBytes(data.getBytes());
				return message;          	
            }
        };

        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
      //informing the user that the content is sending
  	  	System.out.println("############################");
  	  	System.out.println("Sending a new message..... \n\n");    
        if(!(jmsTemplate.equals(null))){
        	jmsTemplate.send(mailboxDestination, message2);
            jmsTemplate.send(mailboxDestination, message3);
            jmsTemplate.send(mailboxDestination, message1);
	        //jmsTemplate.send(mailboxDestination,(MessageCreator) testM);

        }
        else{
        	context.close();
        }
        
        
    }

}
