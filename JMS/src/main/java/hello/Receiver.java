package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;

public class Receiver {

    /**
     * Get a copy of the application context
     */
    @Autowired
    ConfigurableApplicationContext context;

    /**
     * When you receive a message, print it out, then shut down the application.
     * @param message
     */
    public void receiveMessage(String message) {
    	System.out.println("Received Message:\n <" + message + ">\n\n");
    	System.out.println("#############################################");
    }
    
    public void receiveMessage(byte [] message){
    	String str = "";
    	for(int i=0;i<message.length;i++){
    		if(message.equals(null)){
    			str+="";
    		}
    		str+= message[i];
    	}
    	System.out.println("<Received Message:\n "+ str+ " >\n\n");
    	System.out.println("#############################################");

    }
}
