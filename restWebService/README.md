To run in eclipse:
- mvn clean install, to build war file
- Right click on the project in eclipse and Run As.. -> Run on Server
	- This step assumes you have a server such as Tomcat setup on your eclipse
- This should run the restful webapp on your server
- Point your browser to http://localhost:8080/restful/echo/{name}


