package example.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Hello {

    private String date;
    private String message;
    
    // No arg constructor required for jaxb
    public Hello() {
    }

    public Hello(String message, String date) {
        this.message = message;
        this.date = date;
    }
    
	@XmlElement
	public String getDate() {
        return date;
    }

	@XmlElement
    public String getMessage() {
        return message;
    }
    
    public void setDate(String date){
    	this.date=date;
    }
    
    public void setMessage(String mes){
    	this.message=mes;
    }
}