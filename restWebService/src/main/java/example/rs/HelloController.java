package example.rs;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import example.model.Hello;
import example.services.HelloServiceImpl;


public class HelloController {

	private HelloServiceImpl helloServiceImpl = new HelloServiceImpl();
	
	@Path("/{input}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@GET
	public Hello getUserInput(@PathParam("input") @DefaultValue("World") String input){
//		return helloServiceImpl.getInput(input);
		return helloServiceImpl.getInput(input);
	}
	
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@POST
	public Response addUserInput( @Context final UriInfo uriInfo,
			@PathParam( "input" ) @DefaultValue("World") final String input) {
		helloServiceImpl.getInput(input);
		return Response.created( uriInfo.getRequestUriBuilder().path( input ).build() ).build();
	}
	
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@POST
	public Hello addUserXml(@PathParam( "input" ) @DefaultValue("World") final String input) {
		return helloServiceImpl.getInput(input);
	}
}
