package example.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.jws.WebService;

import example.model.Hello;

@WebService(endpointInterface = "example.services.HelloService",
serviceName="helloService")
public class HelloServiceImpl implements HelloService {
	private  final String template = "Hello, %s!";
	static String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());

	public Hello getInput(String message){
		Hello out=new Hello(String.format(template, message),timeStamp);
		return out;

	}

}