package example.services;

import javax.jws.WebMethod;
import javax.jws.WebService;

import example.model.Hello;

@WebService
public interface HelloService {

    @WebMethod
	public Hello getInput(String message);
    
}