package Movies.movie.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
//import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection="movie")
public class Movie 
{
	
	@Id
	private String id;
	private String title;
	private String genre;
	private int year;

	//id for database purposes
	public void setId(String i){
		this.id=i;
	}
	public String getId(){
		return id;
	}
	
	//title
	public void setTitle(String t){
		this.title=t;
	}
	//genre
	public void setGenre(String g){
		this.genre=g;
	}
	//year
	public void setYear(int y){
		this.year=y;
	}
	
	//returning title
	public String getTitle(){
		return title;
	}
	//returning genre
	public String getGenre(){
		return genre;
	}
	//returning year
	public int getYear(){
		return year;
	}

	public Movie(String id, String title, String genre, int year) {
		super();
		this.id=id;
		this.title = title;
		this.genre = genre;
		this.year=year;
	}

	@Override
	public String toString() {
		return " Movie : [ ID: " +id+ ", \t Title: " + title + ", \t Genre: " + genre + ", \t Year: " + year + "]\n";
	}

}