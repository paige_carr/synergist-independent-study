package Movies.movie.core;

import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.apache.log4j.Logger;

import Movies.movie.model.Movie;
//import Movies.movie.repository.Repository;
//import Movies.movie.repository.RepositoryImpl;
import Movies.movie.repository.MovieRepository;

public class App {
	
	public static void main(String []args){
		testData();
	}
	
	public static void testData(){
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		MovieRepository r1= context.getBean(MovieRepository.class);

		
		r1.save(new Movie("1","The Notebook", "Drama", 2010));
		r1.save(new Movie("2","The Marines", "Action", 2002));
		r1.save(new Movie("3","Friends With Benefits", "Comendy", 2012));
		r1.save(new Movie("4","Win Win", "Drama", 2013));
		
		System.out.println("##############################INITIAL CONTENT:############################## \n"+r1.findAll());
		
		Movie update=r1.findById("1");
		update.setTitle("The Notebook: Based on a True Story");
		r1.save(update);
		
		
		System.out.println("\n#################UPDATES OCCURRED:##############################\n"+ r1.findById("1").getTitle()+"\n"
				+r1.findById("1").getGenre()+"\n"+r1.findById("1").getYear()+"\n");
		System.out.println("##############################CHANGED CONTENT:############################## \n"+r1.findAll());
		
		r1.delete("2");
		System.out.println("##############################CHANGED CONTENT:############################## \n"+r1.findAll());
		
		System.out.println("####################################SIZE--#####################################");
		System.out.println("The total number of elements in the database is: "+ r1.count());

	}

}