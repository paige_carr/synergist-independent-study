//package Movies.movie.repository;
//
//import java.util.List;
//
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.data.mongodb.core.query.Update;
//
//import com.mongodb.WriteResult;
//import Movies.movie.model.Movie;
//
//public class RepositoryImpl implements Repository<Movie>{
//	MongoTemplate template;
//	int size;
//	
//	public void setTemplate(MongoTemplate temp){
//		this.template=temp;
//	}
//	
//	public List<Movie> getAllMovies(){
//		return template.findAll(Movie.class);
//		
//	}
//	
//	public void saveMovie(Movie movie){
//		template.insert(movie);
//		size++;
//	}
//	
//	public int getSize(){
//		return size;
//	}
//	
//	public Movie getMovie(String id){
//		return template.findOne(new Query(Criteria.where("id").is(id)),Movie.class);
//	}
//	
//	public WriteResult updateMovie(String id, String title){
//		return template.updateFirst(new Query(Criteria.where("id").is(id)),Update.update("title", title), Movie.class);
//	}
//	
//	public WriteResult updateM(String id, String genre){
//		return template.updateFirst(new Query(Criteria.where("id").is(id)),Update.update("genre", genre), Movie.class);
//	}
//	
//	public void deleteMovie(String id){
//		template.remove(new Query(Criteria.where("id").is(id)),Movie.class);
//		size--;
//	}
//	
//	public void createCollection(){
//		if(!template.collectionExists(Movie.class)){
//			template.createCollection(Movie.class);
//		}
//	}
//	
//	public void dropCollection(){
//		if(template.collectionExists(Movie.class)){
//			template.dropCollection(Movie.class);
//		}
//	}
//}