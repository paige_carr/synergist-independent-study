package Movies.movie.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import Movies.movie.model.Movie;

public interface MovieRepository extends MongoRepository<Movie, String> {
	
	public Movie findById(String Id);

	public List<Movie> findByGenre(String genre);
	
	public List<Movie> findByTitle(String title);
	
	public List<Movie> findByYear(int year);
	
	public List<Movie> findByYearGreaterThanEqual(int year);
	
	
}
