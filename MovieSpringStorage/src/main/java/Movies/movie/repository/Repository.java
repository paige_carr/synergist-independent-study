//package Movies.movie.repository;
//
//
//import java.util.List;
//
//import Movies.movie.model.Movie;
//
//import com.mongodb.WriteResult;
//
//public interface Repository <M>{
//	public List<M> getAllMovies();
//	public void saveMovie(Movie movie);
//	public int getSize();
//	public Movie getMovie(String id);
//	public WriteResult updateMovie(String id, String title);
//	public WriteResult updateM(String id, String genre);
//	public void deleteMovie(String id);
//	public void createCollection();
//	public void dropCollection();
//}