package Movies.movie;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import Movies.movie.model.Movie;
import Movies.movie.repository.MovieRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class AppTest {
	
	@Autowired
	MovieRepository repo;
	
	@Before
	public void setup() {
		repo.save(new Movie("1", "The Notebook", "Drama", 2010));
		repo.save(new Movie("2", "The Fast and The Furoius", "Action", 2002));
		repo.save(new Movie("3","The Roommate", "Comendy", 2012));
		repo.save(new Movie("4","Win Win", "Drama", 2013));
		
		
	}
	
	@After
	public void teardown() { 
		repo.deleteAll();//removing all content from the repository.

	}
	
	@Test
	public void testCreateMovie() {
		Movie newMovie = new Movie("100", "Test Movie", "Test Genre", 2014);
		
		// Save movie
		repo.save(newMovie);
		
		// Retrieve newly saved movie by id
		Movie retrievedMovie = repo.findOne("100");
		
		// Confirm movie was found
		assertNotNull(retrievedMovie);
		
		// Confirm that the retrieved movie is the same as the original movie
		assertEquals(newMovie.getId(), retrievedMovie.getId());
	}
	
	@Test
	public void testFindByGenre() {
		List<Movie> movieList = repo.findByGenre("Drama");
		assertNotNull(movieList);
		
		assertEquals(2, movieList.size());//this is because there are two movies in the drama genre
		
		Movie m = movieList.get(0);
		
		assertEquals("1", m.getId());
		assertEquals("The Notebook", m.getTitle());
		assertEquals("Drama", m.getGenre());
		assertEquals(2010, m.getYear());
		
	}

	
	// Todo: Need to update against what is in the database
	@Test
	public void testUpdateMovie() {
		Movie update= repo.findById("3");
		assertNotNull(update);
		update.setGenre("Horror");
		repo.save(update);
		
		
		assertEquals("3", update.getId());
		assertEquals("The Roommate", update.getTitle());
		assertEquals("Horror", update.getGenre());
		assertEquals(2012, update.getYear());
	
	}
	
	@Test
	public void testFindById() {
		Movie movie=repo.findById("3");
		assertNotNull(movie);
		assertEquals("3", movie.getId());
		assertEquals("The Roommate", movie.getTitle());
		assertEquals("Comendy", movie.getGenre());
		assertEquals(2012, movie.getYear());
	
	}
	
	// Todo: Where is the confirmation?
	@Test
	public void testDeleteMovie() {
		repo.delete(repo.findById("4"));
		
		
	}
	
	@Test
	public void testFindByTitle() {
		List<Movie> movieList =repo.findByTitle("The Roommate");//finding the movie according to the title
		assertNotNull (movieList);//proving that the list actually contains content
		assertEquals(1, movieList.size());//this is proving that the size is equal to 1
		
		Movie m=movieList.get(0);//getting the first element in the movieList
		assertEquals("3", m.getId());//asserting that the id of the movie retrieved matches the id
		assertEquals("The Roommate",m.getTitle());//asserting that the id of the movie retrieved matches the title
		assertEquals("Comendy", m.getGenre());//asserting that the id of the movie retrieved matches the genre
		assertEquals(2012, m.getYear());//asserting that the id of the movie retrieved matches the year
		
	}
	
	@Test
	public void testFindByYear() {
		List<Movie> movieList=repo.findByYear(2013);
		assertNotNull(movieList);
		assertEquals(1, movieList.size());
		
		Movie m=movieList.get(0);
		assertEquals("4", m.getId());
		assertEquals("Win Win", m.getTitle());
		assertEquals("Drama", m.getGenre());
		assertEquals(2013, m.getYear());
		
	
	}
	
	
	@Test
	public void testFindByYearGreaterThanEqualUsingSpring() {
		List<Movie> movies = repo.findByYearGreaterThanEqual(1990);
		
		assertNotNull(movies);
		assertEquals(4, movies.size());
		
		movies = repo.findByYearGreaterThanEqual(2013);

		assertNotNull(movies);
		assertEquals(1, movies.size());
	}
	
	

}
