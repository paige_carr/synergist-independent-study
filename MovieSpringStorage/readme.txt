1. Assuming that you have mongodb installed, open a terminal and enter mongod.
2. Once this has started successfully, import the project into eclipse
3. Right click on the file and select maven install
4. Everything should return back to be successful.
5. Navigate to the App.java class within the MovieSpringStorage. 
6. Right click on the project again and select run as java application.