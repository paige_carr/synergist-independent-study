package cxf.jaxws.requestor;


import cxf.jaxws.provider.HelloWorld;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class ServiceRequestor {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("requestor-beans.xml");
		HelloWorld requestor = (HelloWorld) context.getBean("HelloWorld");
		System.out.println("Hello: " + requestor);
		System.exit(0);
	}
	public void trial(){
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("requestor-beans.xml");
		HelloWorld requestor = (HelloWorld) context.getBean("HelloWorld");
		System.out.println("Hello: " + requestor);
		System.exit(0);
	}
}






