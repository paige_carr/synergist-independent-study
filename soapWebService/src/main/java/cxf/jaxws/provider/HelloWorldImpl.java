package cxf.jaxws.provider;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
//import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.Holder;

@WebService(
		endpointInterface="cxf.jaxws.provider.HelloWorld",
		serviceName="HelloWorldService",
		portName="HelloWorldPort",
		targetNamespace="http://provider.jaxws.cxf/")
@SOAPBinding(
		style=Style.DOCUMENT,
		use=Use.LITERAL,
		parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class HelloWorldImpl implements HelloWorld {
	@WebMethod
	public String sayHello(Holder<String> input) {
		return "Hello, "+ input.value;
	}
}
