package cxf.jaxws.provider;


import javax.jws.WebMethod;
import javax.jws.WebParam;
//import javax.jws.WebParam.Mode;
import javax.jws.WebService;
//import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.Holder;

@WebService
public interface HelloWorld {
	@WebMethod
	String sayHello(@WebParam(name="input", header=true, mode = WebParam.Mode.INOUT)Holder<String> input);

	
}